import { put, takeEvery } from 'redux-saga/effects'
import { select } from 'redux-saga/effects'
import { addMessage } from "./actions/chatActions"

// worker Saga: will be fired on ADD_MESSAGE actions and add a response a alternative fun way to solve this problem
function* botResponde(action) {
 
  const state = yield select();

  let foundBots = state.user.users.filter((u) => {
    if (u.isBot && action.data.text.toUpperCase().includes(u.name.toUpperCase())) {
      return true;
    } else {
      return false;
    }
  }); 
  if (foundBots.length) {
    yield put(addMessage(foundBots[0], "Hey i'm a bot "));
  }
}

function* botSaga() {
  yield takeEvery("ADD_MESSAGE", botResponde);
}

export default botSaga;