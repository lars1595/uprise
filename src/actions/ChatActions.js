
export function addMessage(user, text) {
  return {
    type: "ADD_MESSAGE",
    data: {user: user, date : new Date().getTime(), text : text}
  }
}

export function getMessages(text) {
  return {
    type: "GET_MESSAGE"
  }
}

export function addUser(user) {
  return {
    type: "ADD_USER",
    user: user
  }
}

export function getUsers(text) {
  return {
    type: "GET_USERS"
  }
}

export function updateStatus(user) {
  return {
    type: "UPDATE_STATUS",
    name: user.name,
    status : user.status
  }
}

export function login(payload) {
  return {
    type: "LOGIN",
    name: payload.name,
    avatar: payload.avatar
  }
}

export function getMyUser( ) {
  return {
    type: "GET_MY_USER"  
  }
}