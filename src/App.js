import React, { Component } from 'react';
import './App.css';
import Chat from './views/Chat';
import Send from './views/Send';
import Users from './views/Users';
import Login from './views/Login';
import { connect } from "react-redux"
import { addUser, updateStatus, addMessage } from "./actions/chatActions"

class App extends Component {
  constructor(messages) {
    super();
  
  }

  componentWillMount() {
    const bot1 ={ name: "Bot 1", avatar: "1.png", status: "Online", isBot : true };
    const bot2 ={ name: "Bot 2", avatar: "2.png", status: "Away", isBot : true };
    const bot3 ={ name: "Bot 3", avatar: "2.png", status: "Online", isBot : true };
    this.props.dispatch(addUser(bot1));
    this.props.dispatch(addUser(bot2));
    this.props.dispatch(addUser(bot3));

    setInterval(() => {
      const r = Math.floor(Math.random() * 5) + 1
      if(r === 1) 
        this.props.dispatch(updateStatus({ name: "Bot 3", status: "Away" }));
      if(r === 2) 
        this.props.dispatch(updateStatus({ name: "Bot 3", status: "Online" }));
      if(r === 4) 
        this.props.dispatch(addMessage(bot3, "Hello whats up"));
      if(r === 5) 
        this.props.dispatch(addMessage(bot2, "Hello whats down"));
    }, 1000)
  }


  render() {
    return ('name' in this.props.user  ?
      <div className="main"> 
        <div className="chat-uses-wrapper">
          <Chat messages={this.messages} /><Users users={this.userList} />
        </div>
        <Send />
      </div> :
      <div className="main">
        <Login/>
      </div> 
    );
  }
}

export default App = connect((store) => {
  return { 
     user: store.login.myUser,
  };
})(App)

 
