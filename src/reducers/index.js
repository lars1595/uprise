import { combineReducers } from 'redux'
import chat from './chat'
import user from './user'
import login from './login'

const rootReducer = combineReducers({
  chat,
  user,
  login
})

export default rootReducer