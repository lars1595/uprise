
export default function reducer(state={
    messages: [], 
  }, action) {

    switch (action.type) { 
      case "ADD_MESSAGE" : {
        return {
          ...state,
          messages: [...state.messages, action.data],
        }
      }
      case "GET_MESSAGE" : {
        return {
          ...state,
          messages: [...state.messages],
        }
      }
      default:{}
    }

    return state
}