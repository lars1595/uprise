
export default function reducer(state = {
  users: [],
}, action) {

  switch (action.type) {
    case "ADD_USER": {
      return {
          ...state,
        users: [...state.users, action.user],
      }
    }
    case "UPDATE_STATUS": {
      const users = state.users.map((u) => {
        if (u.name === action.name) {
          return Object.assign({}, u, {
            status: action.status
          })
        }
        return u;
      })
      return {
          ...state,
        users: users
      }
    }
    case "GET_MY_USER": {
      return {
          ...state,
        users: [...state.users]
      }
    }
    default:{}

  }

  return state
}