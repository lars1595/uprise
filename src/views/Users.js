import React, { Component } from 'react';
import { getUsers } from "../actions/chatActions"
import { connect } from "react-redux"

class Users extends Component {

    componentWillMount() {
        this.props.dispatch(getUsers());
    }
    render() {
        let list = this.props.users.map((u) => {
            return <User user={u} key={u.name} />;
        })
        return (<ul className="users">{list}</ul>)
    }
}
export default Users = connect((store) => {
    return {
        users: store.user.users,
    };
})(Users)

class User extends Component {

    render() {
        return (
            <li className="user">
                <div className="row"> 
                    <div className={'col-md-2 status ' + this.props.user.status} >
                        O
                    </div>
                    <div className="col-md-2 avatar" >
                        <img alt="userImg" src={this.props.user.avatar} /><br />
                    </div>
                    <div className="col-md-6">
                        <div className="name" >
                            {this.props.user.name}
                        </div>
                        <div>
                            {this.props.user.status}
                        </div>
                    </div>
                </div>
            </li>
        );
    }
}

