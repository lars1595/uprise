import React, { Component } from 'react';
import { connect } from "react-redux"
import { getMessages } from "../actions/chatActions"


class Chat extends Component {

  componentWillMount() {
    this.props.dispatch(getMessages());
  }

  render() {
    let list = this.props.messages.map((message) => {
      return <li key={message.user.name +message.date }><span className="user">{message.user.name}</span>  {message.text}</li>;
    })

    return (<ul className="chat">{list}</ul>);
  }
}

export default Chat = connect((store) => {
  return {
    messages: store.chat.messages,
  };
})(Chat)
