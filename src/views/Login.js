import React, { Component } from 'react';
import { connect } from "react-redux"
import { login, addUser } from "../actions/chatActions"

class Login extends Component {
    constructor() {
        super();
        this.state = { name: '', avatar: '1.png', doLogin: true };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleSubmit(e) {
        if (this.state.name) {
            this.props.dispatch(login({ name: this.state.name, avatar: this.state.avatar }));
            this.props.dispatch(addUser({ name: this.state.name, avatar: this.state.avatar, status: "Online" }));
            this.setState({ doLogin: false });
        }

        e.preventDefault();
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    render() {

        return (this.state.doLogin ? <div className="login form-group">
            <div className="shade"></div>
            <label htmlFor="name">Name </label>
            <input className="form-control" type="text" name="name" value={this.state.name} onChange={this.handleChange} />
            <label htmlFor="avatar">Avatar </label>

            <select className="form-control" onChange={this.handleChange} name="avatar" value={this.state.avatar} >
                <option value="1.png">Male</option>
                <option value="2.png">Female</option>
            </select>
            <button className="btn btn-primary" onClick={this.handleSubmit.bind(this)}>Login</button></div> : null);
    }
}

export default Login = connect((store) => {
    return {
    };
})(Login)