import React, { Component } from 'react';
import { connect } from "react-redux"
import { addMessage, getMyUser } from "../actions/chatActions"

class Send extends Component {
  constructor() {
    super();
    this.state = { text: '' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(getMyUser());
  }


  handleSubmit(e) {
    if (this.state.text)
      this.props.dispatch(addMessage(this.props.user, this.state.text));

    this.setState({
      text: ''
    });
    e.preventDefault();
  }

  handleChange(e) {
    this.setState({ text: e.target.value });
  }
  render() { 
    return ('name' in this.props.user  ? <div className="send">   <input className="form-control" type="text" value={this.state.text} onChange={this.handleChange} />
      <button className="btn btn-primary" onClick={this.handleSubmit.bind(this)}>Send</button></div> : null);
  }
}

export default Send = connect((store) => {
  return {
    user: store.login.myUser,
  };
})(Send)