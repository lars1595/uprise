import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux"
import './index.css';
import App from './App';
import reducer from "./reducers"
import createSagaMiddleware from 'redux-saga'
import registerServiceWorker from './registerServiceWorker';
import { createStore, applyMiddleware } from 'redux'
import mySaga from './sagas'

const app = document.getElementById('root')
const sagaMiddleware = createSagaMiddleware()
const store = createStore(reducer, applyMiddleware(sagaMiddleware))
sagaMiddleware.run(mySaga)
ReactDOM.render(<Provider store={store}>
  <App />
</Provider>, app);
 
registerServiceWorker();
